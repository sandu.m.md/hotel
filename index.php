<?php
if (true)
{
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
} 
else 
{
    error_reporting(0);
    ini_set('display_errors', 0);
}


require_once(__DIR__ . "/core/autoload.php");

$data = json_decode('{"username":"sandu", "password":"sandu"}');

$method = isset($_GET["method"]) ? $_GET["method"] : "Web";
$action = isset($_GET["action"]) ? $_GET["action"] : "def";

if (isset($argv[1]))
{
    $method = $argv[1];
    // $action = $argv[2];
}

$controller = ucfirst(strtolower($method));

if (!class_exists($controller))
{
    $controller = $defaultMethod;
}

$controller::start($action, $data);


?>