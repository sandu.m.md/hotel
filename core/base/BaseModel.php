<?php

class BaseModel extends Base
{
    protected $isChanged;
    protected $sql;
    protected $result;

    function __construct()
    {
        $this->isChanged = false;
        $this->result = false;
    }

    public function get()
    {
        if (!$this->isChanged && $this->result)
        {
            $this->isChanged = false;
            return $this->result;
        }

        if (!(Database::query($this->getQuery())))
        {
            $this->errMsg = Database::getErrMsg();
            return false;
        }

        $this->result = Database::getObjectResult();
        return $this->result;
    }

    public function setQuery($sql)
    {
        $this->isChanged    = true;
        $this->sql          = $sql;
    }

    public function getQuery()
    {
        return $this->sql;
    }

}

?>