<?php

class BaseStatic 
{
    protected static $errCode;
    protected static $errMsg;

    public static function getErrCode()
    {
        return self::$errCode;
    }

    public static function getErrMsg()
    {
        return self::$errMsg;
    }
}

?>