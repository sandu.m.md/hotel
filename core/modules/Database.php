<?php

class Database extends BaseStatic
{
    private static $link = null;
    private static $conf;
    private static $result = false;
    private static $objectResult = false;

    public static function init($conf)
    {
        self::$conf = $conf;
    } 

    public static function getObjectResult()
    {
        if (!self::$result)
        {
            return false;
        }

        if (self::$objectResult)
        {
            return self::$objectResult;
        }

        if (mysqli_num_rows(self::$result) == 1)
        {
            return json_decode(json_encode(mysqli_fetch_assoc(self::$result)));
        }
        
        $res = array();

        while($row = mysqli_fetch_assoc(self::$result))
        {
            if (isset($row["id"]))
            {
                $id = $row['id'];
                unset($row['id']);
                $res[$id] = $row;
            } 
            else 
            {
                $res[] = $row;
            }
        }

        self::$objectResult = json_decode(json_encode($res));

        return self::$objectResult;
    }

    public static function query($sql)
    {
        // var_dump($sql);
        self::$objectResult = false;
        self::$result = self::request($sql);
        return self::$result;
    }

    public static function request($query)
    {
        if (!self::reconnect())
        {
            return false;
        }

        if (!($result = mysqli_query(self::$link, $query)))
        {
            return false;
        }

        if (!is_object($result))
        {
            return true;
        }
        
        return mysqli_num_rows($result) ? $result : false;
    }

    public static function connect()
    {
        $conf = &self::$conf;
        if (!(is_object($conf) && isset($conf->host) && isset($conf->user) && isset($conf->pass) && isset($conf->db) && $conf->host != "" && $conf->user != "" && $conf->pass != "" && $conf->db != ""))
        {
            self::$errMsg = "Wrong database config!";
            return false;
        }

        if (!(self::$link = mysqli_connect(self::$conf->host, self::$conf->user, self::$conf->pass, self::$conf->db)))
        {
            self::$errMsg = "Database connect error! : " . mysqli_connect_error() . " : " . mysqli_connect_errno();
            return false;
        }

        return true;
    }

    private static function isConnected()
    {
        return mysqli_ping(self::$link) ? false : true;
    }

    private static function reconnect()
    {
        if (is_null(self::$link) || self::isConnected())
        {
            return self::connect();
        }

        return true;
    }

}


