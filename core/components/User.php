<?php

class User
{
    private static $id;
    private static $username;
    private static $password;
    private static $firstName;
    private static $lastName;
    private static $email;


    public static function init($info)
    {
        self::$username = isset($info->username) ? $info->username : "";
        self::$password = isset($info->password) ? $info->password: "";
        self::$id = isset($info->id) ? $info->id : "";
        self::$firstName = isset($info->firstName) ? $info->firstName: "";
        self::$lastName = isset($info->lastName) ? $info->lastName: "";
        self::$email = isset($info->email) ? $info->email: "";
    }

    public static function login()
    {
        if (!($user = Users::get("username='{username}' AND password='{password}'", array("{username}" => self::$username, "{password}" => self::$password))))
        {
            return false;
        }

        self::init($user);

        return true;
    }

    public static function register()
    {
        // checks

        if (!Users::add(array("username" => "'". self::$username ."'", "password" => "'". self::$password ."'")))
        {
            return false;
        }

        return true;
    }


}

?>