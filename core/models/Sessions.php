<?php

class Sessions
{
    private $result;

    public static function tableName()
    {
        return "_sessions";
    }

    public static function get($filter = "", $params = array())
    {
        $sql = "SELECT * FROM " . self::tableName();

        if ($filter != "")
        {
            $sql .= " WHERE " . str_replace(array_keys($params), $params, $filter);
        }

        return self::exec($sql);
    }

    public static function add($fields)
    {
        $sql = "INSERT INTO " . self::tableName() . "(" . implode(",", array_keys($fields)) . ") VALUES (" . implode(",", $fields) . ")";

        return self::exec($sql);
    }

    public static function update($update, $uFields, $filter = "", $fFields = array())
    {
        $sql = "UPDATE " . self::tableName() . " SET " . str_replace(array_keys($uFields), $uFields, $update);

        if ($filter != "")
        {
            $sql .= " WHERE " . str_replace(array_keys($fFields), $fFields, $filter);
        }

        return self::exec($sql);
    }

    public static function exec($sql)
    {
        if (!Database::query($sql))
        {
            self::$errMsg = Database::getErrMsg();
            return false;
        }

        return Database::getObjectResult();
    }
}

?>