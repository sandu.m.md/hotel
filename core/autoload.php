<?php
require_once(__DIR__ . "/base/Base.php");
require_once(__DIR__ . "/base/BaseModel.php");
require_once(__DIR__ . "/base/BaseStatic.php");
require_once(__DIR__ . "/components/User.php");
require_once(__DIR__ . "/conf/Conf.php");
require_once(__DIR__ . "/controllers/Test.php");
require_once(__DIR__ . "/controllers/Wrong.php");
require_once(__DIR__ . "/controllers/Auth.php");
require_once(__DIR__ . "/controllers/Main.php");
require_once(__DIR__ . "/controllers/Web.php");
require_once(__DIR__ . "/models/Locations.php");
require_once(__DIR__ . "/models/Images.php");
require_once(__DIR__ . "/models/Users.php");
require_once(__DIR__ . "/modules/Database.php");

?>