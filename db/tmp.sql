CREATE DATABASE Hotel;
USE Hotel;

CREATE TABLE `locations`
(
    `id` INT AUTO_INCREMENT PRIMARY KEY,
    `title` VARCHAR(200),
    `description` VARCHAR(1000)
    
);

CREATE TABLE `images`
(
    `id` INT AUTO_INCREMENT PRIMARY KEY,
    `path` VARCHAR(200)
);

CREATE TABLE `locations_images`
(
    `id` INT AUTO_INCREMENT PRIMARY KEY,
    `location_id` INT,
    `image_id` INT
);